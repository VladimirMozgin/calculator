Rails.application.routes.draw do
  root 'credits#index'
  post '/calculator', to: 'credits#calculator'
end
