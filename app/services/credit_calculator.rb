# Calculate credit by annuity or simple method
class CreditCalculator
  def initialize(params)
    @time = params[:time].to_i
    @percent = params[:percent].to_i
    @credit = params[:credit].to_i
  end

  def calculate
    statistic = []
    debt = @credit
    1.upto(@time) do |i|
      assessed_percent = (debt * p).round(2)
      amortization_debt = (month_pay + (method_calculate_cof) * assessed_percent).round(2)
      statistic << {
        month: i, debt: debt, assessed_percent: assessed_percent,
        month_pay: month_pay, amortization_debt: amortization_debt
      }
      debt = (debt - amortization_debt).round(2)
    end
    statistic
  end

  private

  def p
    @p ||= (@percent * 0.01) / 12
  end

  def calculate_cof
    fail NotImplementedError, 'Sorry, you have to override cof'
  end

  #if we want use more difficult way for solution this cof will stay more difficult too
  def month_pay
    fail NotImplementedError, 'Sorry, you have to override month_pay'
  end
end
