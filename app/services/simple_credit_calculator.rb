# Calculate credit by simple method
class SimpleCreditCalculator < CreditCalculator
  private

  def method_calculate_cof
    1
  end

  def month_pay
    @month_pay ||= (@credit.to_f / @time).round(2)
  end
end
