# Calculate credit by annuity method
class AnnuityCreditCalculator < CreditCalculator
  private

  def method_calculate_cof
    -1
  end

  def month_pay
    @month_pay ||= (@credit * (p + p / (((1 + p)**@time) - 1))).round(2)
  end
end
