angular.module('calculator', []).
  controller('calculatorCtrl', function($scope, $log, calculatorService){
    $scope.percent = 0;
    $scope.time = 0;
    $scope.credit = 0;
    $scope.method = 'annuity';
    $scope.statistic = [];
    $scope.calculator = function() {
      calculatorService.getStatistic($scope.percent, $scope.time, $scope.credit, $scope.method).
        success(function(data){
          $scope.statistic = data;
        }).
        error(function(error){
          $log.log(error);
        });
    }
  }).
  service('calculatorService', function($http) {
    this.getStatistic = function (percent, time, credit, method) {
      return $http.post('calculator', {
        percent: percent,
        time: time,
        credit: credit,
        method: method
      });
    }
  });

