class CreditsController < ApplicationController
  def calculator
    calc = register_calculator
    @statistics = calc.calculate
    render json: @statistics
  end

  private

  def register_calculator
    if params[:method] == 'annuity'
      AnnuityCreditCalculator.new(params)
    elsif params[:method] == 'simple'
      SimpleCreditCalculator.new(params)
    end
  end
end
