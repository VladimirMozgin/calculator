require 'rails_helper'

describe CreditsController do
  describe 'POST calculator' do
    context 'response annuity method' do
      let(:calculator_annuity_request) do
        post :calculator, time: 3, percent: 10, credit: 100, method: 'annuity'
      end
      let(:annuity_result) do
        [
          {
            'month' => 1, 'debt' => 100, 'assessed_percent' => 0.83,
            'month_pay' => 33.89, 'amortization_debt' => 33.06
          },
          {
            'month' => 2, 'debt' => 66.94, 'assessed_percent' => 0.56,
            'month_pay' => 33.89, 'amortization_debt' => 33.33
          },
          {
            'month' => 3, 'debt' => 33.61, 'assessed_percent' => 0.28,
            'month_pay' => 33.89, 'amortization_debt' => 33.61
          }
        ]
      end
      let(:annuity_calculator_response) { JSON.parse response.body }

      before { calculator_annuity_request }

      it 'should have status 200' do
        expect(response.status).to eq(200)
      end

      context 'should have statistic' do
        it { expect(annuity_calculator_response.count).to eq 3 }
        it { expect(annuity_calculator_response).to eq annuity_result }
      end
    end

    context 'response simple method' do
      let(:calculator_simple_request) do
        post :calculator, time: 3, percent: 10, credit: 100, method: 'simple'
      end
      let(:simple_result) do
        [
          {
            'month' => 1, 'debt' => 100, 'assessed_percent' => 0.83,
            'month_pay' => 33.33, 'amortization_debt' => 34.16
          },
          {
            'month' => 2, 'debt' => 65.84, 'assessed_percent' => 0.55,
            'month_pay' => 33.33, 'amortization_debt' => 33.88
          },
          {
            'month' => 3, 'debt' => 31.96, 'assessed_percent' => 0.27,
            'month_pay' => 33.33, 'amortization_debt' => 33.60
          }
        ]
      end
      let(:simple_calculator_response) { JSON.parse response.body }

      before { calculator_simple_request }

      it 'should have status 200' do
        expect(response.status).to eq(200)
      end

      context 'should have statistic' do
        it { expect(simple_calculator_response.count).to eq 3 }
        it { expect(simple_calculator_response).to eq simple_result }
      end
    end
  end
end
