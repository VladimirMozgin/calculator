require 'rails_helper'

describe CreditCalculator do
  describe 'annuity statistic' do
    let(:annuity_calculator) do
      AnnuityCreditCalculator.new(time: 3, percent: 10, credit: 100)
    end
    let(:annuity_statistic) { annuity_calculator.calculate }
    let(:annuity_result_table) do
      [
        {
          month: 1, debt: 100, assessed_percent: 0.83,
          month_pay: 33.89, amortization_debt: 33.06
        },
        {
          month: 2, debt: 66.94, assessed_percent: 0.56,
          month_pay: 33.89, amortization_debt: 33.33
        },
        {
          month: 3, debt: 33.61, assessed_percent: 0.28,
          month_pay: 33.89, amortization_debt: 33.61
        }]
    end
    it { expect(annuity_statistic.count).to eq 3 }
    it { expect(annuity_statistic).to eq annuity_result_table }
  end

  describe 'simple statistic' do
    let(:simple_calculator) do
      SimpleCreditCalculator.new(time: 3, percent: 10, credit: 100)
    end
    let(:simple_statistic) { simple_calculator.calculate }
    let(:simple_result_table) do
      [
        {
          month: 1, debt: 100, assessed_percent: 0.83,
          month_pay: 33.33, amortization_debt: 34.16
        },
        {
          month: 2, debt: 65.84, assessed_percent: 0.55,
          month_pay: 33.33, amortization_debt: 33.88
        },
        {
          month: 3, debt: 31.96, assessed_percent: 0.27,
          month_pay: 33.33, amortization_debt: 33.6
        }
      ]
    end
    it { expect(simple_statistic.count).to eq 3 }
    it { expect(simple_statistic).to eq simple_result_table }
  end
end
