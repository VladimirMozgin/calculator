require 'rails_helper'

describe 'POST /calculator' do
  it do
    expect(post: '/calculator').to route_to(controller: 'credits',
                                            action: 'calculator')
  end
end
